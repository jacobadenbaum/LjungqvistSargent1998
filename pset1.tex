\documentclass[header.tex]{subfiles}

\usepackage{fancyhdr}
\pagestyle{fancy}

\def\course{Econ 8581}
\def\name{Jacob Adenbaum}
\lhead{\today}
\chead{\course}
\rhead{\name}
\title{Homework 1}
\author{\name\\
\course}

\begin{document}
\maketitle 
\section{Model}
This homework asks us to solve the simplified Ljungqvist and Sargent
(1998) model presented in the slides.  In this model, a continuum of
finitely-lived workers with linear utility search for jobs drawn from an
exogenous wage offer distribution $F(w)$, searching with some search
intensity $s \in [0,1]$ which enables them to sample the wage offer
distribution with probability $\pi(s)$.  They are laid off with
probability $\lambda$, die with probability $\alpha$, accumulate human
capital $h$, and are paid $wh$, which is proportional to their marginal product. 

We assume that human capital lies on a grid $h \in \left\{ \underbar h,
\underbar h + \Delta, \dots, \overline h
\right\}$.  Their human capital declines by $\psi_U \Delta$ if they are
unemployed, and $\psi_F \Delta$ if they are fired (where both $\psi_U,
\psi_F \in \mathbb N$). If they are employed, their human capital
increased by $\Delta$ per period.  When they are unemployed, workers
receive benefits $b$ every period, paid by the government.  The
government finances these benefits through a tax $\tau$ on all take home
pay (including unemployment benefits).  

Define the effective discount rate $\hat \beta = \beta (1-\alpha)$.
Workers value unemployment at a given level of human capital according
to a value function $U(h)$ which must satisfy the Bellman equation
\begin{equation}
    \begin{aligned}
        U(h) = \max_s \; & (1-\tau) b - c(s) + \hat \beta \left[ 
                \pi(s) \int_w \max\left\{ W(h, h'), U(h') \right\}dF(w)
                + (1-\pi(s)) U(h') 
            \right]\\
            \text{s.t. }  
            & h' = \max\left\{ h - \psi_U \Delta, \underbar h \right\}
        \end{aligned}
    \label{unemployed_bellman}
\end{equation}

Similarly, workers who are offered a wage $w$ and have human capital $h$
will value this offer at $W(w, h)$, where $W$ must satisfy the Bellman
equation 
\begin{equation}
    W(w, h) = (1-\tau) wh + \hat \beta \left[ 
        (1-\lambda) W(w, \min\left\{h + \Delta, \overline h  \right\})
        + \lambda U(\max\left\{ h-\psi_F \Delta, \underbar h \right\})
    \right]
    \label{employed_bellman}
\end{equation}
We want to find a tax rate $\tau$, as well as value functions $U(h)$ and
$W(w,h)$ such that given $\tau$, equations \eqref{unemployed_bellman}
and \eqref{employed_bellman} hold, and that the government's tax
revenues are high enough to finance the benefits being paid out in at
the equilibrium unemployment rate.  

\section{Computing the Equilibrium}

My code for this problem set is written primarily in Julia.  It is split
into three pieces
\begin{enumerate}[label=\arabic*.]
    \item \verb|Grids.jl| and \verb|GridFuns.jl|: These modules (which
        were actually written for another project) provide simple and
        efficient storage, indexing, and evaluation of grids and
        grid-constrained functions.  

    \item \verb|LS98.jl|: This file contains all of the code necessary
        to compute an equilibrium of the model.  
        
        \begin{enumerate}[label=\arabic*.]
            \item It defines a type \verb|LS98| which contains all of
                the model parameters and the corresponding value
                functions, as well as appropriate methods to implement
                value function iteration given and to compute the
                optimal tax rates in a nested fixed point loop.   More
                documentation is provided in the doc strings of each
                function, but at a high level, the methods provided are:
                \begin{itemize}
                    \item \verb|solve_taxes!| provides the outer loop for the
                        bisection algorithm to determine the equilibrium tax
                        rate $\tau$ that balances the budget

                    \item \verb|solve_bellman!| provides the outer loop for our
                        implementation of value function iteration.  It is
                        repeatedly called by \verb|solve_taxes!|

                    \item \verb|simulate!| implements our simulation of the
                        economy.  It is called repeatedly by \verb|solve_taxes!|

                    \item \verb|budget| computes the per-capita budget
                        deficit of our model economy from the simulation
                        results.  It is called repeatedly by
                        \verb|solve_taxes!|

                    \item \verb|update_value!| iterates our value functions one
                        step per function call.  It computes and returns the
                        relative error for each of the two value functions.  It
                        is called repeatedly by \verb|solve_bellman!|

                    \item \verb|update_employment| iterates the value function
                        for employed workers one step with each function call,
                        looping over all of the grid points for wages and human
                        capital.  It is called repeatedly by
                        \verb|update_value!|

                    \item \verb|update_unemployment| iterates the value function
                        for unemployed workers one step with each function call,
                        looping over each value of human capital, computing the
                        optimal search decision, and updating the value function
                        accordingly.  It is called repeatedly by
                        \verb|update_value!|

                    \item \verb|optimal_search| computes the optimal search
                        intensity for a given level of human capital by
                        repeatedly trisecting the grid of available search
                        intensities and exploiting the concavity of the search
                        maximization problem to exclude one of the three
                        regions.  Called repeatedly by
                        \verb|update_unemployment|

                    \item \verb|unemployed_search| computes the value to a
                        worker with human capital $h$ of searching with
                        intensity $s$.  Called repeatedly by
                        \verb|optimal_search|
                \end{itemize}

            \item We also provide a type \verb|Agent| which is used in
                the agent-based simulation of the model economy.  Each
                agent consists of a reference to a solved model economy,
                a wage $w$ (which can be set to -1 for unemployed
                workers) and a level of human capital $h$.  It has only
                one method implemented:
                \begin{itemize}
                    \item \verb|tomorrow!(a::Agent)| iterates the agent
                        forward one period, drawing random numbers as
                        appropriate to resolve the uncertainty.  The
                        \verb|simulate| method operates by constructing
                        agents, repeatedly calling this method to iterate
                        them forward, and saving their state at every
                        iteration.  

                        Note that if the agent happens to die in a given
                        period, this method reinitializes them as a
                        brand new agent, unemployed, with a random level
                        of human capital drawn from the grid)
                \end{itemize}

            \item 
        \end{enumerate}

    \item The top level script \verb|pset1.jl| which can be run to
        execute the full code from top to bottom and create all of the
        output.  
\end{enumerate}

\section{Results}

I compute the equilibrium for the economy with the specified
parameters, using a relative tolerance threshold of 1e-3 for determining
the convergence of the value function iteration.  I find that the
equilibrium tax rate will be $\tau = 0.351\%$ and that the unemployment
rate for the economy will be 4.54\%. In Figure \ref{fig:1} I plot the
optimal search effort policy function of the agents in the economy, and
in Figure \ref{fig:2} I plot the agent's reservation wage function.
Although the workers in the economy appear to adjust their reservation
wage in response to changes in their human capital (with low human
capital workers and high human capital workers both demanding relatively
high reservation wages, and medium human-capital workers demanding
comparatively lower reservation wages), almost all of the unemployed
workers appear to be at a corner solution for their optimal search
effort.  All but the lowest human capital workers search as hard as they
possibly can.  This is likely because the human capital costs of remaining
unemployed are so high, they all want to exit unemployment as fast as
possible.  

I have experimented with using a higher tolerance level, and the results
do not appear to change much.  

\begin{figure}[h]
    \centering
    \includegraphics{output/search_effort.pdf}
    \caption{Agents' Optimal Search Effort}
    \label{fig:1}
\end{figure}

\begin{figure}[h]
    \centering
    \includegraphics{output/reservation_wage.pdf}
    \caption{Agent's Optimal Reservation Wage}
    \label{fig:2}
\end{figure}

\end{document}
