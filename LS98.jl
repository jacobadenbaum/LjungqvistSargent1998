
using GridFuns
using Distributions

########################################################################
#################### Model Primitives ##################################
########################################################################

# Search Effort Cost
c(s) = 0.5*s

# Probability of Contact
p(s) = s^0.3

# Wage Distribution
const Wd    = Truncated(Normal(0.5, sqrt(0.1)), 0, 1)
F(w)        = cdf(Wd, w)
f(w)        = pdf(Wd, w)


mutable struct LS98{T, Q, S}
    β::T
    α::T
    λ::T
    ψF::Int
    ψU::Int
    b::T
    Δ::T
    hg::Grid{T,1}
    sg::Grid{T,1}
    wg::Grid{T,1}
    dF::Vector{Float64}
    F::Vector{Float64}
    τ::T
    U::GridFun{T, 1, Q}
    W::GridFun{T, 2, S}
    s::GridFun{T, 1, Q}
end

# A constructor using keyword arguments
function LS98(;β=0.9985, α=0.0009, λ=0.009, ψF=30, ψU=10, b=0.1, τ=0.5,
              h̲=1.0, h̅=2.0, hgsize=201, 
              s̲=0.0, s̅=1.0, sgsize=41,
              w̲=0.0, w̅=1.0, wgsize=41)
    
    hg = linspace(h̲,h̅,hgsize)  |> Grid
    Δ  = (h̅-h̲)/hgsize

    wg = linspace(w̲,w̅, wgsize) |> Grid
    sg = linspace(s̲,s̅, sgsize) |> Grid
    
    # Make the functions
    U  = GridFun(hg, zeros(hgsize))
    W  = GridFun(cat(wg, hg), zeros(wgsize,hgsize))
    s  = GridFun(hg, zeros(hgsize))

    # Make the distribution of wage offers (discretized)
    dF = [f.(w) for w in wg]
    dF = dF./sum(dF)
    F  = cumsum(dF)
    
    return LS98(β, α, λ, ψF, ψU, b, Δ, hg, wg, sg, dF, F, τ, U, W, s)
end

########################################################################
#################### Solving the Model #################################
########################################################################


"""
```
unemployed_search(m::LS98, h, s)
```
Using the value functions on the current iteration, this function
computes the value of being unemployed with a level of human capital `h`
if the worker chooses to search with search intensity `s`.  It follows
the Bellman Equation

`` U(h | s) = (1-τ) b - c(s) + ∫_w \\max(W(w, h'), U(h')) dF(w) ``
"""
function unemployed_search(m::LS98, h, s)
    
    # Unpack Model
    τ = m.τ
    b = m.b
    β = m.β
    α = m.α
    h̲,h̅ = bounds(m.hg)
    ψU= m.ψU
    Δ = m.Δ
    dF= m.dF
    β̂ = (1-α)*β
    
    # Get the flow today
    flow = (1-τ)*b - c(s)

    # Get the continuation tomorrow
    hp = max(h-ψU*Δ, h̲)
    w1 = 0.0
    for (w, df) in zip(m.wg, m.dF)
        w1 += max(m.W((w, hp)), m.U(hp))*df
    end

    return flow + β̂*(p(s)*w1 + (1-p(s))*m.U(hp))
end

"""
```
sidx, U = optimal_search(m::LS98, h)
```
Computes the value of unemployment given that the worker has human
capital `h` by maximizing over the possible choices for search
intensity.  
"""
function optimal_search(m::LS98, h)
    return concave_max(s->unemployed_search(m, h, s), m.sg) 
end

function update_unemployment(m::LS98)
    U = zeros(size(m.U))
    s = zeros(size(m.s))
    for (i, h) in enumerate(m.U)
        
        # Solve the optimal search problem
        sidx, Uval = optimal_search(m, h)
        s[i] = m.sg[sidx]
        U[i] = Uval
    end
    return U, s
end


"""
```
update_employment(m)
```
Iterates once on the Bellman Equation for employed workers (which
conveniently does not involve a maximization problem)
"""
function update_employment(m::LS98)
    
    # Unpack Model
    τ = m.τ
    b = m.b
    β = m.β
    α = m.α
    h̲,h̅ = bounds(m.hg)
    ψU= m.ψU
    ψF= m.ψF
    Δ = m.Δ
    dF= m.dF
    λ = m.λ
    β̂ = (1-α)*β
    
    # Make new functions
    W = zeros(size(m.W))

    for (i, (w, h)) in enumerate(m.W)
        # Flow Utility Today
        flow = (1- τ)*w*h
        # Continuation Value Tomorrow
        cont = (1-λ)*m.W((w, min(h+Δ, h̅)))
        cont+= λ*m.U(max(h-ψF*Δ, h̲))
        # Update Value of Working
        W[i] = flow + β̂*cont
    end

    return W
end

"""
```
errW, errU = update_value!(m)
```
Performs a single step of Value Function Iteration on the bellman
equations for employed and unemployed workers simultaneously, and
returns the relative errors for both.  
"""
function update_value!(m::LS98)
    U, s = update_unemployment(m)
    W    = update_employment(m)
    
    errW = norm(m.W.values - W)/norm(m.W.values)
    errU = norm(m.U.values - U)/norm(m.U.values)

    # Update the value functions
    copy!(m.U, U)
    copy!(m.W, W)
    copy!(m.s, s)
    
    return errW, errU
end


"""
```
solve_bellman!(m; reltol=1e-3)
```
Performs value function iteration for an instance `m` of type `LS98` by
repeatedly calling the method `update_value!` on `m` until the relative
errors (for both bellman equations) returned fall below the specified
tolerance `reltol`
"""
function solve_bellman!(m::LS98; reltol=1e-3)
    
    converged = false
    itercount = 1
    while !converged
        errW, errU = update_value!(m)
        converged  = (errW < reltol) & (errU < reltol)
        itercount += 1
        print("\rError on Iteration $itercount is $(max(errW, errU))")
    end
    print("\n")
end


function reservation_wage(m::LS98, h)
    
    U = m.U(h)
    W = [m.W((x, h)) for x in m.wg]
    idx = searchsortedfirst(W, U)
    return m.wg[idx]
end

########################################################################
#################### Simulate The Economy ##############################
########################################################################

mutable struct Agent{T,S,Q}
    m::LS98{T,S,Q}
    w::Float64
    h::Float64
end

function Agent(m::LS98)
    # Starts off unemployed with a random human capital 
    w = -1.0
    h =  m.hg[rand(1:length(m.hg))]
    return Agent(m, w, h)
end


"""
```
w, h = tomorrow!(a::Agent)
```
This function simulates a day in the life of a worker, updates their
state (their wage and their human capital), and returns it. 
"""
function tomorrow!(a::Agent)
    
    # Unpack the model
    m    = a.m
    h̲, h̅ = bounds(m.hg)

    # Check whether they die.  If they do, they get replaced by a brand
    # new worker!  
    if rand() < m.α
        a.w = -1.0
        a.h =  m.hg[rand(1:length(m.hg))]
    # If they're employed
    elseif a.w >= 0.0
        # Check whether they are laid off
        if rand() < m.λ
            a.w = -1.0
            a.h = max(a.h - m.ψF*m.Δ, h̲)
        # Otherwise they stay employed
        else
            a.h = min(a.h + m.Δ, h̅)
        end
    # If they're unemployed, they sample the wage distribution, and they
    # get to choose whether or not they want to take the new job
    else
        # See where their human capital will be tomorrow
        hp  = max(a.h - m.ψU*m.Δ, h̲)
        
        # If they work hard, they might get a wage offer
        s   = m.s(a.h)
        a.h = hp

        if rand() < p(s)
            # Give them a new wage offer
            idx = searchsortedfirst(m.F, rand())
            w   = m.wg[idx]
            
            # They take the job if it's nice
            if m.W((w, hp)) > m.U(hp) 
                a.w = w
            end
        end
    end
    return a.w, a.h
end


"""
```
W, H = simulate!(a::Agent, T=100, burn=100)
```
Simulate an agent for `T+burn` periods, and throw out the first `burn`
periods before returning their history of wages and human capital.    
"""
function simulate!(a::Agent, T=100, burn=100)
    
    W = zeros(T)
    H = zeros(T)
    
    for t=1:T+burn
        W[t], H[t] = tomorrow!(a)
    end
    
    return W[burn+1:end], H[burn+1:end]
end

"""
```
W, H = simulate!(m::LS98, [W, H]; N=10000, T=100, burn=100, rng=1234)
```
Simulates an economy with `N` agents over `T`+`burn` periods, returning
only the results for the final `T`.  Allows the user to set a random
seed `rng` so that multiple simulations will all work with the same random
draws.  

The provided matrices `W` and `H` must both have size `T` by `N`, and
this function modifies them in place.  If preallocated matrices are not
provided, they will be constructed first.  
"""
function simulate!(m::LS98, W, H; N=10000, T=100, burn=100, rng=1234)
    
    # Set the seed
    srand(rng)

    # Check that the matrices are the correct size
    msg = "Incorrectly Sized W and H matrices"
    all(size(W) .== size(H) .== (T, N)) || error(msg)
    
    for i=1:N
        # Make a new worker
        a = Agent(m)
        
        # Life doesn't start till after grad school 
        for t=1:burn
            tomorrow!(a)
        end
        
        # Now we start recording their history
        for t=1:T
            W[t,i], H[t,i] = tomorrow!(a)
        end
    end

    return W, H
end

function simulate(m::LS98; N=10000, T=100, kwargs...)
    
    W = zeros(T,N)
    H = zeros(T,N)

    return simulate!(m, W, H; N=N, T=T, kwargs...)
end

########################################################################
#################### Solve for Taxes ###################################
########################################################################


"""
```
budget(m::LS98, W, H)
```
Given matrices `W` and `H` with the results from a simulation of the
economy, computes and returns the per-capita budget deficit for the
government, averaged over all of the periods.   
"""
function budget(m::LS98, W, H)
    
    t = 0.0
    for (w, h) in zip(W, H)
        if w < 0
            t += w*(1 - m.τ)*m.b
        else
            t += m.τ*w*h
        end
    end
    return t/length(W)
end


"""
```
solve_taxes!(m::LS98; tol=1e-5, reltol=1e-3, N=10000, T=100, kwargs...)
```
Solves for the equilibrium tax rate of the economy by
    1) Guessing a tax rate for the economy
    2) Solving for the model's equilibrium given that tax rate through
        value function iteration
    3) Simulating the economy to compute government's per-capita budget
        deficit in that equilibrium
    4) Adjusting our guess of the correct tax rate, and repeating steps
        2 - 3 with the new guess
We continue to iterate this way until the per-capita budget deficit
falls below the given tolerance threshold `tol`.  

All remaining keywords arguments are passed through to the simulation.  
"""
function solve_taxes!(m::LS98; tol=1e-5, reltol=1e-3, N=10000, T=100, kwargs...)
    
    # Preallocate the matrices
    W = zeros(T, N)
    H = zeros(T, N)

    # Start with a tax rate of 0.5
    m.τ = 0.5
    low = 0.0
    high= 1.0
    
    while true
        
        println("Solving the model for τ=$(m.τ)")
        solve_bellman!(m; reltol=reltol)
        simulate!(m, W, H; N=N, T=T, kwargs...) 
        b = budget(m, W, H)
        println("Budget Surplus is $b\n")
        
        # Check if we've converged to within the correct tolerance
        if abs(b) < tol
            break
        end
        
        # If the gov't runs a surplus, then taxes are too high
        if b > 0
            high    = m.τ
            m.τ     = (low + m.τ)/2
        else # Taxes are too low
            low = m.τ
            m.τ = (high + m.τ)/2
        end
        
    end

    return m.τ

end


########################################################################
#################### Concave Maximization over Discrete Sets ###########
########################################################################

function concave_max(f::Function, list)::Tuple{Int, Float64}
    concave_max(f, list, 1, length(list))
end

function concave_max(f::Function, list, a, b)::Tuple{Int, Float64}
    fa = f(list[a])::Float64
    fb = f(list[b])::Float64
    return concave_max(f, list, a, b, fa, fb)
end

"""
Computes the maximum of a concave function `f` on the given `list` by
repeatedly trisecting the list and ruling out of of the three segments.
(Similar in spirit to a bisection algorithm).  
"""
function concave_max(f::Function, list, a::Int, b::Int, fa::Float64,
                     fb::Float64)::Tuple{Int, Float64}
    # Handle The Corner Cases
    if b - a == 2
        c  = a + 1
        fc = f(list[c])

        fm = max(fa, fb, fc)
        if fa == fm
            return a, fa
        elseif fb == fm
            return b, fb
        else
            return c, fc
        end
    elseif b - a == 1
        if fa > fb
            return a, fa
        else
            return b, fb
        end
    elseif b == a
        return a, fa
    else # The Answer is somewhere in the interior
        # Get the new index to try
        c1 = floor(Int, a + (b-a)/3)
        c2 = floor(Int, a + (b-a)*2/3)
        
        # Compute the function value at the midpoint
        fc1 = f(list[c1])
        fc2 = f(list[c2])

        s1 = (fc1 - fa) >= 0.0
        s2 = (fc2 - fc1) >= 0.0
        s3 = (fb - fc2) >=0.0 
        
        if s1 & s2 & s3
            # The answer lies between c2 and b
            return concave_max(f, list, c2, b, fc2, fb)
        elseif s1 & s2 & !s3
            # The answer lies between c1 and b
            return concave_max(f, list, c1, b, fc1, fb)
        elseif s1 & !s2 & !s3
            # The answer lies between a and c2
            return concave_max(f, list, a, c2, fa, fc2)
        elseif !s1 & !s2 & !s3
            # The answer lies between a and c1
            return concave_max(f, list, a, c1, fa, fc1)
        else
            error("The problem is not concave")
        end
    end
end

