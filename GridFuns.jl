module GridFuns
    
    using Reexport
    include("Grids.jl")    
    @reexport using .Grids
    import Base:    start, next, done, getindex, setindex!, length,
                    size, copy!
    import Grids.locate
    export GridFun, getindex, setindex!, locate

    struct GridFun{T,N,Q<:AbstractArray{S,N} where S}
        grid::Grid{T, N}
        values::Q
    end

    function GridFun{T,N,Q}(grid, values) where {T,N,Q} 
        size(grid) == size(values) || error("Dimension Mismatch")
        return new(grid, values)
    end

    function locate(gf::GridFun, x)
        # Find the location of the x value on the grid
        loc = Grids.locate(gf.grid, x)

        return loc
    end
    
    # Note that this function is unsafe if you apply it to locations
    # that are not actually on the grid.  
    (gf::GridFun)(x) = begin
        # Locate the point on the grid
        loc = locate(gf, x)
        return gf.values[loc]
    end
    @inline (gf::GridFun)(x::Vararg{N,T}) where {N,T} = gf(x)
    
    # Indexing
    getindex(gf::GridFun, x...)    = gf.values[x...]
    function setindex!(gf::GridFun, value, key...)
        gf.values[key...] = value
    end
    
    # Pass Iteration down to the underlying grid object
    start(gf::GridFun)          = start(gf.grid)
    next(gf::GridFun, state)    = next(gf.grid, state)
    done(gf::GridFun, state)    = done(gf.grid, state)
    length(gf::GridFun)         = length(gf.grid)
    size(gf::GridFun)           = size(gf.values)
    copy!(gf, values)           = copy!(gf.values, values)
end
