using PyPlot

# Tolerance Parameter
reltol = 1e-3

# Code for the LS98 Model
include("LS98.jl")

# Initialize the model
m = LS98()

# Solve the model when τ = 0.5
solve_bellman!(m; reltol=reltol)

# Solve For Balanced Budget Taxes
τ = solve_taxes!(m; reltol=reltol)

# Calculate unemployment Rate
W, H = simulate(m)
u = mean(W .== -1.0)

# Plot the reservation wage
fig, ax = subplots()
hg      = [h for h in m.hg]
rw      = reservation_wage.(m, hg)
plot(hg, rw, 
    label="Reservation Wage")
xlabel("Human Capital")
ylabel("Wage")
title("Reservation Wage")
savefig("output/reservation_wage.pdf")

# Plot the search effort as a function of human capital
fig, ax = subplots()
hg      = [h for h in m.hg]
s       = [m.s(h) for h in hg]
plot(hg, s)
xlabel("Human Capital")
ylabel("Intensity")
title("Search Effort")
savefig("output/search_effort.pdf")

