
module Grids

########################################################################
#################### Grid Implementation ###############################
######################################################################## 

import  Base: getindex, start, next, done, push!,size, length, in,
        endof

export  Grid, length, size, dim, bounds, locate, plot_grid

struct Grid{T, N}
    points::NTuple{N,Vector{T}}
    dims::NTuple{N, Int}
end

# Empty Grid
Grid(T::Type) = Grid{T,0}(NTuple{0, 
                                 Vector{T}}(Vector{T}()), 
                                  NTuple{0, Int}())

# Grids return themselves
Grid(g::Grid) = g

# Other constructors
function Grid(points::Matrix{T}) where {T}
    k = size(points,2)
    gpoints= NTuple{k, Vector{T}}(points[:,i] for i=1:k)
    gdims  = NTuple{k, Int}(length(p) for p in gpoints)
    return Grid(gpoints, gdims)
end

function Grid(points::NTuple{N, Vector{T}}) where {N,T}
    dims = NTuple{N,Int}(length(p) for p in points)
    return Grid(points, dims)
end

function Grid(p1)
    return Grid((collect(p1),))
end

function Grid(p1, points...)
    g1 = Grid(collect(p1))
    return cat(g1, Grid(points...))
end

function getindex(g::Grid, idx::Int)
    return g[ind2sub(g.dims,idx)]
end

# Treat multiple integers as a vector
getindex(g::Grid, idx...) = g[[idx...]]

function getindex{T, N}(g::Grid{T,N}, idx)
    
    # Dimension Checking
    length(idx)  == N || begin
        l = length(idx)
        msg = "Index has dimension $l but the grid has dimension $N"
        throw(DimensionMismatch(msg))
    end
    
    # Get the point from the list of points
    point = [g.points[d][i] for (d,i) in enumerate(idx)]
    return point
end

function getindex{T}(g::Grid{T,1}, idx::Int)
    # Get the point from the list of points
    return g.points[1][idx] 
end

function getindex(g::Grid{T,1}, idx::CartesianIndex{1}) where T
    return g.points[1][idx]
end


## Make the grids iterable
start(g::Grid) = 1 
function next(g::Grid, idx)
    # Get the current value
    val = g[idx]
    return val, idx+1
end
done(g::Grid, idx) = idx > prod(g.dims)


length(g::Grid)         = prod(g.dims)
Base.size(g::Grid)      = g.dims
dim(g::Grid)            = length(g.dims)
bounds(g::Grid) = (minimum.(g.points), maximum.(g.points)) .|> collect
bounds(g::Grid{T, 1}) where T = (minimum(g.points[1]), 
                                 maximum(g.points[1])) 
endof(g::Grid)  = CartesianIndex(g.dims)

# Make Cartesian Indexes Iterable
start(i::CartesianIndex) = 1
next(i::CartesianIndex, s) = i[s], s+1
done(i::CartesianIndex{N}, s) where N = s > N

# Find a point's location on the grid
function locate(g::Grid{T,N}, x) where {T,N}
    
    # Check the dimension
    length(x) == N || begin
        msg = "Dimension of x must be $N.  It has length $(length(x))"
        throw(DimensionMismatch(msg))
    end
        
    loc = NTuple{N, Int}(searchsortedfirst(p,v) for (p,v) in
                         zip(g.points, x))
    return loc |> CartesianIndex{N}
end

locate(g::Grid, x...) = locate(g, x)

## Add new dimensions to the grid
function push{T,N}(g::Grid{T,N}, x)
    gpoints = NTuple{N+1}(g.points..., x)
    return Grid(gpoints...)
end

function in(x, g::Grid{T,N}) where {T,N}
    (length(x) == N) || return false
    for i=1:N
        if !(x[i] in g.points[i])
            return false 
        end
    end
    
    return true
end

function Base.cat(g1::Grid{T,N}, g2::Grid{S,M}) where {T,N,S,M}
    gpoints = promote(g1.points..., g2.points...)
    return Grid(gpoints)
end

## Plotting Grids
plot_grid(g::Grid, dim) = reshape(map(x->x[dim], g), g.dims...)

end
